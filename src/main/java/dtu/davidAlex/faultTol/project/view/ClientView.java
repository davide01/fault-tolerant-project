package dtu.davidAlex.faultTol.project.view;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.beans.PropertyChangeListener;

import javax.swing.JFrame;
import javax.swing.JTextField;

import dtu.davidAlex.faultTol.project.controller.ClientController;
import dtu.davidAlex.faultTol.project.controllerImpl.ClientControllerImpl;

import javax.swing.JLabel;
import javax.swing.Action;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JRadioButton;

public class ClientView {

	private JFrame frame;
	private JTextField textField;
	private ClientController clientController;
	private JTextField errorReplaceTextField;
	// private JTextField errorAddTextField;
	private JLabel lblReplace;
	// private JLabel lblAdd;
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ClientView window = new ClientView();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public ClientView() {
		clientController = new ClientControllerImpl();
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		textField = new JTextField();
		textField.setBounds(69, 42, 306, 30);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		JLabel lblEnterText = new JLabel("Enter text:");
		lblEnterText.setBounds(68, 22, 131, 15);
		frame.getContentPane().add(lblEnterText);
		
		final JRadioButton rdbtnCrc = new JRadioButton("CRC");
		rdbtnCrc.setBounds(250, 144, 149, 23);
		rdbtnCrc.setEnabled(false);
		frame.getContentPane().add(rdbtnCrc);
		
		final JRadioButton rdbtnHammingCode = new JRadioButton("Hamming code");
		rdbtnHammingCode.setBounds(250, 171, 149, 23);
		rdbtnHammingCode.setEnabled(false);
		frame.getContentPane().add(rdbtnHammingCode);
		
		JCheckBox chckbxUseErrorChecking = new JCheckBox("Use error checking");
		chckbxUseErrorChecking.setBounds(250, 117, 183, 23);
		chckbxUseErrorChecking.addItemListener(new ItemListener() {
			
			public void itemStateChanged(ItemEvent e) {
				if(e.getStateChange() == 1) {
					rdbtnCrc.setEnabled(true);
					rdbtnCrc.setSelected(true);
					rdbtnHammingCode.setEnabled(true);
					errorReplaceTextField.setEnabled(true);
					// errorAddTextField.setEnabled(true);
					lblReplace.setEnabled(true);
					// lblAdd.setEnabled(true);
				} else {
					rdbtnCrc.setEnabled(false);
					rdbtnHammingCode.setEnabled(false);
					errorReplaceTextField.setEnabled(false);
					// errorAddTextField.setEnabled(false);
					lblReplace.setEnabled(false);
					// lblAdd.setEnabled(false);
				}
			}
		});
		frame.getContentPane().add(chckbxUseErrorChecking);
		
		ButtonGroup buttonGroup = new ButtonGroup();
		buttonGroup.add(rdbtnCrc);
		buttonGroup.add(rdbtnHammingCode);
		
		
		JButton buttonSend = new JButton("Send msg");
		buttonSend.setBounds(250, 198, 149, 23);
		buttonSend.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				String msg = null;
				if(!(msg = textField.getText()).equals("")) {
					String replace = null;
					// String add = null;
					if(!(replace = errorReplaceTextField.getText()).equals("")){
						clientController.setReplaceErrors(replace);
					}
					// if(!(add = errorAddTextField.getText()).equals("")) {
					// 	clientController.setAddErrors(add);
					// }
					clientController.setMessage(msg);
					if(rdbtnCrc.isSelected()) {
						clientController.doErrorCheck(false);;
					} else if(rdbtnHammingCode.isSelected()) {
						clientController.doErrorCheck(true);;
					}
					clientController.sendMessage();
				}
			}
		});
		frame.getContentPane().add(buttonSend);
		
		
		errorReplaceTextField = new JTextField();
		errorReplaceTextField.setBounds(31, 147, 114, 19);
		errorReplaceTextField.setEnabled(false);
		frame.getContentPane().add(errorReplaceTextField);
		errorReplaceTextField.setColumns(10);
		
		// errorAddTextField = new JTextField();
		// errorAddTextField.setBounds(31, 201, 114, 19);
		// errorAddTextField.setEnabled(false);
		// frame.getContentPane().add(errorAddTextField);
		// errorAddTextField.setColumns(10);
		
		lblReplace = new JLabel("Enter positions to replace");
		lblReplace.setBounds(31, 117, 200, 15);
		lblReplace.setEnabled(false);
		frame.getContentPane().add(lblReplace);
		
		// lblAdd = new JLabel("Enter positions to add");
		// lblAdd.setBounds(31, 174, 200, 15);
		// lblAdd.setEnabled(false);
		// frame.getContentPane().add(lblAdd);
	}
}
