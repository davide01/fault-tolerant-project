package dtu.davidAlex.faultTol.project.controllerImpl;

import dtu.davidAlex.faultTol.project.controller.ServerController;
import dtu.davidAlex.faultTol.project.errorCheck.ErrorCheck;

public class ServerControllerImpl implements ServerController{

	private String message;

	/**
	 * Sets the recieved message so it can be decoded.
	 * @param message is the String containing the received message.
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * Decodes and prints out the recieved message according to the encoding scheme used.
	 */
	public void decodeMessage() {
		if(message.charAt(0) == '1') {
			int index = message.indexOf("x");
			String crc = message.substring(1, index);
			String text = message.substring(index + 1);
			if(!ErrorCheck.crc(text).equals(crc)) {
				System.out.println("CRC's are differenet. Error happened during transmission.");
			} else {
				System.out.println("Recieved message: " + text);
			}
		} else if(message.charAt(0) == '2') {
			// hamming code
			
			// removes the 2 from the beginning used to tell the server it is hamming code
			String hammingString = message.substring(1);
			byte hammingMessage[] = ErrorCheck.hexStringToByteArray(hammingString);
			// System.out.println("Revieved message: " + message);
			// System.out.println("Recieved Hamming String: " + hammingString);
			int errors = ErrorCheck.checkHammingCode(hammingMessage);

			if(errors == 0) {
				// no errors
				System.out.println("The hamming code found that there were no errors.");
				String decodedMessage = ErrorCheck.extractHammingMessage(hammingMessage);
				System.out.println("Message Recieved: " + decodedMessage);
			} else {
				if(errors == 1) {
					// max of 1 error per byte found.  Can be corrected
					System.out.println("The hamming code found that there was an error in the message but it could be corrected.");
					System.out.println("...Correcting Error...");

					byte correctedHammingCode[] = ErrorCheck.correctHammingCode(hammingMessage);
					String decodedMessage = ErrorCheck.extractHammingMessage(correctedHammingCode);

					System.out.println("Corrected Message: " + decodedMessage);
				} else {
					if(errors == 2) {
						// error detected.  Cannot be fixed.
						System.out.println("The hamming code found that there was an error in the message that could not be corrected.");
					} else {
						// should never get here
						System.out.println("The message had more errors than should have been detectable by the hamming code.");
					}
				}
			}
			
		} else {
			System.out.println("Recieved message: " + message);
		}
	}

}
