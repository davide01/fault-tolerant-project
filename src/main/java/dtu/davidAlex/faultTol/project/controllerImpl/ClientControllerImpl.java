package dtu.davidAlex.faultTol.project.controllerImpl;

import dtu.davidAlex.faultTol.project.client.Client;
import dtu.davidAlex.faultTol.project.controller.ClientController;
import dtu.davidAlex.faultTol.project.errorCheck.ErrorCheck;

public class ClientControllerImpl implements ClientController {

	private StringBuilder message;

	private Client client;

	private String errorCheck;

	private String replaceErrors;

	/**
	 * Constructs a ClientControllerImpl object, which starts a new Client.
	 */
	public ClientControllerImpl() {
		client = new Client();
		message = new StringBuilder();
	}

	/**
	 * Appends a prefix character to the message according to the encoding of the message, if the message has any error checking.
	 * Then injects errors into the message according to the locations specified.
	 * Then sends the message to the server.
	 */
	public void sendMessage() {
		if (errorCheck != null) {
			message.insert(0, errorCheck);
		}
		if (replaceErrors != null) {
			injectError();
		}
		client.sendMessage(message.toString());
	}

	/**
	 * Encodes the message to be sent to the server according to the Error Checking algorithm specified by <b>type</b>.
	 * Also marks a variable so the server knows how the message is encoded.
	 * If the encoding is CRC, then "1" is the value marked.
	 * If the encoding is Hamming Code, then "2" is the value marked.
	 * @param type is a boolean indicating how the client should encode the message before it is sent to the server.  True means CRC.  False means Hamming(8,4) Code.
	 */
	public void doErrorCheck(boolean type) {
		if (!type) {
			errorCheck = "1" + ErrorCheck.crc(message.toString()) + "x";
		} else {
			// errorCheck = ErrorCheck.hammingCode(message);
			// 2 is used to indicate hamming code. It needs to be removed before decoding.
			errorCheck = "2";

			// this encodes the message as a hamming code byte array then converts it back
			// to a string so it can be sent.
			// The string needs to be decoded before it can be actually read.
			// It will most likely be gibberish until it is decoded.
			String msg = message.toString();
			byte hammingMessage[] = ErrorCheck.encodeHammingCode(msg);
			msg = ErrorCheck.byteArrayToHexString(hammingMessage);
			message.delete(0, message.length());
			message.append(msg);
		}
	}

	/**
	 * Injects an error into the message after it has been processed by one of the
	 * error checking algorithms. If the code is a crc code then it replaces the
	 * indicies specified in replaceErrrors with the character "e" to indicate an
	 * error. If the code is a hamming code, it flips the bits at the specified
	 * indicies in all bytes. For hamming code, all bytes will have the same error
	 * applied to it. The indices in a byte start at 0 and go left to right
	 */
	public void injectError() {
		if (replaceErrors != null) {
			String errorCheckType = message.toString().substring(0, 1);
			String workingString = message.toString().substring(1); // this removes the number from the beginning
																	// indicating the type of error checking

			if (errorCheckType.equals("1")) {
				byte[] bytesWithError = flipBits(workingString, errorCheckType);
				message.delete(0, message.length());
				message.append(errorCheck);
				message.append(bytesWithError.toString());
			} else {
				// hamming code
				byte[] hammingBytesError = flipBits(workingString, errorCheckType);

				String messageWithError = errorCheckType + ErrorCheck.byteArrayToHexString(hammingBytesError);
				message.delete(0, message.length());
				message.append(messageWithError);
			}

			replaceErrors = null;
		}
	}

	/**
	 * Helper method for flipping bits at specific locations in an array of bytes generated from the String <b>workingString</b>.
	 * @param workingString is the String which will be converted to a byte array and which will have specific bits flipped in each byte of that array according to the locations speficied previously in replaceErrors.
	 * @param errorCheckType is a String indicating which type of encoding the String <b>workingString</b> is encoded in.  "1" for CRC and "2" for Hamming Code.
	 * @return Returns an array of bytes generated from the String workingString, except with bits flipped in all bytes in locations specified by replaceErrors.
	 */
	private byte[] flipBits(String workingString, String errorCheckType) {
		int repInd[] = getErrorsIndexes();
		byte[] sourceBytes = null;
		if(errorCheckType.equals("1")) {
			int index = workingString.indexOf('x');
			
			sourceBytes = workingString.substring(index).getBytes();
		} else {
			sourceBytes = ErrorCheck.hexStringToByteArray(workingString);
		}
		byte flippedBytes[] = new byte[sourceBytes.length];
		
		for (int i = 0; i < sourceBytes.length; i++) {
			flippedBytes[i] = sourceBytes[i];
			for (int index : repInd) {
				if (index < 0 || index > 7) {
					// index out of range abort
					System.out.println("One of the indices in replaceErrors is out of range! Aborting error injection.");
					return null;
				}
				
					// this flips the bit specified by index, with index 0 being the most
					// significant bit and index 7 being the least significant bit.
					flippedBytes[i] = (byte) (flippedBytes[i] ^ ((byte) Math.pow(2, 7 - index)));				
			}
		}
		return flippedBytes;
	}

	/**
	 * Helper Function for the flipBits function.  It parses the String replaceErrors and returns an array of integers containing the locations specified.
	 * @return Returns an array of integers parsed from the values specified in the String replaceErrors.
	 */
	private int[] getErrorsIndexes() {
		String replacementIndices[] = replaceErrors.split(",");
		int repInd[] = new int[replacementIndices.length];

		try {
			for (int i = 0; i < replacementIndices.length; i++) {
				repInd[i] = Integer.parseInt(replacementIndices[i]);
			}
		} catch (NumberFormatException e) {
			System.out.println("One of the indices in replaceErrors is not a number! Aborting error injection.");
			return null;
		}

		return repInd;
	}

	/**
	 * Sets the message to be sent to the server.
	 * @param msg is the String contianing the message tha will be send to the server.
	 */
	public void setMessage(String msg) {
		if (message.length() > 0) {
			message.delete(0, message.length());
		}
		message.append(msg);
	}

	/**
	 * Returns the message that is currently set to be sent to the server.
	 * @return Returns the String containing the message that is set to be sent to the server.
	 */
	public String getMessage() {
		return message.toString();
	}

	/**
	 * Sets the locations which should have bits flipped in the bytes of the encoded message before the message is sent to the server.
	 * @param replace is the String containing the locations which should have bits flipped in the bytes of the encoded message before it is sent to the server.
	 */
	public void setReplaceErrors(String replace) {
		this.replaceErrors = replace;
	}

}
