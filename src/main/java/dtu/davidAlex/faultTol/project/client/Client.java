package dtu.davidAlex.faultTol.project.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

import dtu.davidAlex.faultTol.project.errorCheck.ErrorCheck;

public class Client implements Runnable {
	private PrintWriter writer;
	private Socket socket;
	private Thread client;
	private boolean messageSet;
	private String message;

	/**
	 * Initializes the client
	 */
	public Client() {
		messageSet = false;
		client = new Thread(this);
		try {
			socket = new Socket("localhost", 9000);
			client.start();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/**
	 * Has the client start looking to send the message.
	 */
	public void run() {
		while (true) {
			try {
				writer = new PrintWriter(socket.getOutputStream(), true);
				if (messageSet) {
					messageSet = false;
					writer.println(message);
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Returns true if the message is set.  Returns false otherwise.
	 * @return Returns true if the message is set.  False otherwise.
	 */
	public boolean isMessageSet() {
		return messageSet;
	}

	/**
	 * Sends the message to the server.
	 * @param message is the message to be sent to the server.
	 */
	public void sendMessage(String message) {
		this.message = message;
		this.messageSet = true;
	}

}
