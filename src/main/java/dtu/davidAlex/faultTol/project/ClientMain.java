package dtu.davidAlex.faultTol.project;

import dtu.davidAlex.faultTol.project.client.Client;

/**
 * Runs the Client without the user interface.
 */
public class ClientMain {

	public static void main(String[] args) {
		new Client();
	}

}
