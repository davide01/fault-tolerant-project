package dtu.davidAlex.faultTol.project;

import dtu.davidAlex.faultTol.project.server.Server;

/**
 * Launches the Server
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        new Server();
    }
}
