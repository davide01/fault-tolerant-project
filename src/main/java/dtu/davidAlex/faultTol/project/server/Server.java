package dtu.davidAlex.faultTol.project.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

import dtu.davidAlex.faultTol.project.controller.ServerController;
import dtu.davidAlex.faultTol.project.controllerImpl.ServerControllerImpl;

public class Server implements Runnable {

	ServerSocket outSocket;
	BufferedReader reader;
	Socket socket;
	Thread server;
	ServerController controller;

	/**
	 * Initalizes the Server.
	 */
	public Server() {
		server = new Thread(this);
		controller = new ServerControllerImpl();
		try {
			outSocket = new ServerSocket(9000);
			System.out.println("Server is running and listening..");
			socket = outSocket.accept();
			System.out.println("Client connected.");
			server.start();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Has the server begin looking for a message from the the Client.
	 */
	public void run() {
		String message = null;
		while (true) {
			try {
				reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
				message = reader.readLine();
				if(message == null) {
					System.out.println("Client disconnected. Closing socket and shutting down server.");
					socket.close();
					reader.close();
					break;
				}
				controller.setMessage(message);
				controller.decodeMessage();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

}
