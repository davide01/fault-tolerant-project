package dtu.davidAlex.faultTol.project.errorCheck;

import java.util.ArrayList;

public class ErrorCheck {

	/**
	 * Takes a message and returns the corresponding CRC value.
	 * @param text is the message whose CRC value is to be calculated.
	 * @return Returns the CRC value of the string <b>text</b>.
	 */
	public static String crc(String text) {
		byte[] messageBytes = text.getBytes();
		int crcPolynomial  = 0xAAAAAAAA;   // the basic state
		int divider = 0xABCD3214;   

		for (byte b : messageBytes) {
		    int tmp = (crcPolynomial ^ b) & 0xff;

		    // for every byte
		    for (int i = 0; i < 8; i++) {
				if ((tmp & 1) == 0){
					tmp = (tmp >>> 1);	
				} 
				else {
					tmp = (tmp >>> 1) ^ divider;	
				}                 
		    }
		    crcPolynomial = (crcPolynomial >>> 8) ^ tmp;
		}

		// xor
		crcPolynomial = crcPolynomial ^ 0xffffffff;
		System.out.println("crc calculated: " + crcPolynomial);
		return Integer.toHexString(crcPolynomial);
	}

	/**
	 * Returns an array of bytes, with the string now encoded in Hamming(8,4).
	 * It is the responsibility of the receiver to decode the message, check for and correct single bit errors, and 
	 * check for double bit errors.
	 * @param text is the string which is to be encoded in Hamming(8,4)
	 * @return Returns the byte array containing the Hamming(8,4) encoded message.
	 */
	public static byte[] encodeHammingCode(String text) {
		// for every 4 bits of information in text, there requires 8 bits of hamming code, so we need twice
		// the amount of space.
		byte textBytes[] = text.getBytes();
		byte hammingCode[] = new byte[textBytes.length * 2];
		byte filter = 0x01;

		for(int i = 0; i < hammingCode.length; i++) {
			// bit 0 is the overall parity bit.  It stores the parity of the other 7 bits.
			// bits 1,2,4 are parity bits.  They store the parity of the data bits.
			// bits 3,5,6,7 are data bits.  They store the data.

			// compute data bits
			// filter the data bit out, which depends on if we are in the front half or the back half of the text byte, then shift it by the relevent amount
			byte data[] = new byte[4];
			boolean isFirstHalf = i % 2 == 0;
			data[0] = (byte) ((textBytes[i/2] & ((isFirstHalf)? 0x80 : 0x08)) >>> ((isFirstHalf)? 7 : 3));
			data[1] = (byte) ((textBytes[i/2] & ((isFirstHalf)? 0x40 : 0x04)) >>> ((isFirstHalf)? 6 : 2));
			data[2] = (byte) ((textBytes[i/2] & ((isFirstHalf)? 0x20 : 0x02)) >>> ((isFirstHalf)? 5 : 1));
			data[3] = (byte) ((textBytes[i/2] & ((isFirstHalf)? 0x10 : 0x01)) >>> ((isFirstHalf)? 4 : 0));

			// compute parity bits
			byte parity[] = new byte[4];
			parity[1] = computeParity(data[0],data[1],data[3]); // 3,5,7
			parity[2] = computeParity(data[0],data[2],data[3]); // 3,6,7
			parity[3] = computeParity(data[1],data[2],data[3]); // 5,6,7

			// parity[0] = computeParity(data[1],data[2],data[3]); // the overall pairity

			// sanity check
			for(int j = 0; j < 4; j++) {
				// System.out.printf("0x%02x\n", data[j]);
				if(!isSingleBit(data[j])) {
					System.out.printf("data[%d]: 0x%02x", j, data[j]);
				}
			}
			for(int j = 1; j < 4; j++) {
				// System.out.printf("0x%02x\n", parity[j]);
				if(!isSingleBit(parity[j])) {
					System.out.printf("parity[%d]: 0x%02x", j, parity[j]);
				}
			}

			// manually construct the byte
			byte result = 0x00;
			// result = (byte) (result | (parity[0] << 7));
			result = (byte) (result | (parity[1] << 6));
			result = (byte) (result | (parity[2] << 5));
			result = (byte) (result | (data[0] << 4));
			result = (byte) (result | (parity[3] << 3));
			result = (byte) (result | (data[1] << 2));
			result = (byte) (result | (data[2] << 1));
			result = (byte) (result | data[3]);

			// put the total parity of the other 7 bits as the most significant bit in the byte
			byte totalParity = computeParity(result);
			result = (byte) (result | (totalParity << 7));
			hammingCode[i] = result;
		}

		return hammingCode;
	}

	/**
	 * A sanity check to make sure that variables have all 0's in bits 1-7.  They are allowed to have anything in bit 0.
	 * @param b is the byte to be checked
	 * @return Returns true if <b>b</b> has a value of either 0 or 1.  Returns false otherwise.
	 */
	private static boolean isSingleBit(byte b) {
		return (b == 0 || b == 1);
	}

	/**
	 * Computes the parity of the three bits, passed in as bytes,  and returns it as a byte
	 * Assume the bytes have value of either 0 or 1.
	 * @param b1 is the first byte to take the parity of.
	 * @param b2 is the second byte to take the parity of.
	 * @param b3 is the third byte to take the parity of.
	 * @return Returns the parity of the three bytes <b>b1</b>, <b>b2</b>, and <b>b3</b>.
	 */
	private static byte computeParity(byte b1, byte b2, byte b3) {
		return (byte) ((b1 ^ b2) ^ b3);
	}

	/**
	 * Computes the parity of the byte, sans the most significant digit.  This method computes the parity of the byte using all of the bits indicated by zeros as follows: 1000 0000.  It would exclude the first bit.  This method is indended to be used to compute the overall parity of a byte for use with the overall parity bit in hamming codes.
	 * @param b
	 * @return the parity of the byte, stored in the least significant digit of the returned byte.
	 */
	private static byte computeParity(byte b) {
		byte parity = (byte) ((b & 0x01) ^ ((b & 0x02) >>> 1)); // initialize with the first 2 bits
		int filter = 4;
		for(int i = 2; i < 7; i++) {
			parity = (byte) (parity ^ ((b & filter) >>> i));
			filter = filter * 2;
		}
		return parity;
	}

	/**
	 * Checks if the hamming code in <b>hammingMessage</b> is correct.  I.E. It checks if the parity bits match up with what they should given the data bits.
	 * It counts the maximum number of errors in each byte, that can be detected at least, and returns that.
	 * @param hammingMessage is the byte array of hamming code to be checked.
	 * @return Returns the maximum number of errors that could be detected in at least one of the bytes in <b>hammingMessage</b>.
	 */
	public static int checkHammingCode(byte hammingMessage[]) {
		int max = 0;
		for (int i = 0; i < hammingMessage.length; i++) {
			int check = checkByte(hammingMessage[i]);
			// System.out.printf("\n0x%02x has %d errors", hammingMessage[i], check);
			max = Math.max(max,check);
		}

		return max;
	}

	/**
	 * Helper method to determine the number of errors in a byte.  First it checks the parity bits to determine if it is valid.  If it is valid there are 0 errors.  If it is not valid, it attempts a correction.  If the corrected byte is valid, then there was exactly 1 error.  If the corrected byte is not valid, then there are 2 errors and it cannot be corrected.
	 * @param hammingByte is the byte being examined to determine the number of errors it contains.
	 * @return Returns the number of errors in the byte, 0, 1, or 2.
	 */ 
	private static int checkByte(byte hammingByte) {
		int errors = checkByteHelper(hammingByte);
		if(errors > 0) {
			byte correctedHammingByte = correctHammingByte(hammingByte);
			int errors2 = checkByteHelper(correctedHammingByte);

			// System.out.printf("\n0x%02x corrected to: 0x%02x", hammingByte, correctedHammingByte);
			
			if(errors2 > 0) {
				// correcting the errors didn't make the byte valid.  Therefore, there are 2 errors and they cannot be fixed.
				return 2;
			} else {
				// the error could be corrected to result in a valid byte.  Therefore, there is only 1 error.
				return 1;
			}
		} else {
			// no errors.  Byte is valid
			return 0;
		}
	}
	
	/**
	 * Helper method to a helper method to check a single byte for inconsistancies between data bits and parity bits.  It checks if the parity bits match up with what they should given the data bits.  
	 * @param hammingByte is the byte of hamming code to be checked.
	 * @return Returns the number of parity bits that do not agree with the data bits in byte <b>b</b>.
	 */
	private static int checkByteHelper(byte hammingByte) {
		byte data[] = getDataBits(hammingByte);
		byte parity[] = getParityBits(hammingByte);
		int count = 0;

		// p0,p1,p2,d0,p3,d1,d2,d3

		// 3,5,7
		if (computeParity(data[0],data[1],data[3]) != parity[1]) {
			count++;
		}

		// 3,6,7
		if (computeParity(data[0],data[2],data[3]) != parity[2]) {
			count++;
		}

		// 5,6,7
		if(computeParity(data[1],data[2],data[3]) != parity[3]) {
			count++;
		}

		// p1,p2,p3,d0,d1,d2,d3
		if(computeParity(hammingByte) != parity[0]) {
			count++;
		}

		return count;
		
	}

	/**
	 * Extracts and returns the data bits from the byte <b>hammingByte</b>.
	 * @param hammingByte is the byte from which the data bits are being extracted.
	 * @return Returns a byte array containing the data bits extracted from <b>hammingByte</b>.  Each element is a byte with value of either 0 or 1, corresponding to the value of the respective data bit in <b>hammingByte</b>.  The returned bits corresponds to the data bits in the following order: 3,5,6,7.  I.E.  This returns bits 3,5,6,7 from <b>hammingByte</b>.
	 */
	private static byte[] getDataBits(byte hammingByte) {
		byte data[] = new byte[4];
		data[0] = (byte) ( (hammingByte & 0x10) >>> 4 ); // 3
		data[1] = (byte) ( (hammingByte & 0x04) >>> 2 ); // 5
		data[2] = (byte) ( (hammingByte & 0x02) >>> 1 ); // 6
		data[3] = (byte) ( (hammingByte & 0x01) >>> 0 ); // 7
		return data;
	}

	/**
	 * Extracts and returns the parity bits from the byte <b>hammingByte</b>.
	 * @param hammingByte is the byte from which the parity bits are being extracted.
	 * @return Returns a byte array containing the parity bits extracted from <b>hammingByte</b>.  Each element is a byte with value of either 0 or 1, corresponding to the value of the respective parity bit in <b>hammingByte</b>.  The returned bits correspond to the parity bits in the following order: 0,1,2,4.  I.E. This returns bits 0,1,2,4 from <b>hammingByte</b>.
	 */
	private static byte[] getParityBits(byte hammingByte) {
		byte parity[] = new byte[4];
		parity[0] = (byte) ( (hammingByte & 0x80) >>> 7 ); // p1,p2,p3
		parity[1] = (byte) ( (hammingByte & 0x40) >>> 6 ); // 3,5,7
		parity[2] = (byte) ( (hammingByte & 0x20) >>> 5 ); // 3,6,7
		parity[3] = (byte) ( (hammingByte & 0x08) >>> 3 ); // 5,6,7
		return parity;
	}

	/**
	 * Extracts and returns the string encoded in hamming code from the byte array <b>hammingMessage</b>.
	 * Assumes the byte array has an even number of elements.
	 * This message may not actually be the message that was originally encoded.  This can happen if there was an error in transmission.
	 * @param hammingMessage is the byte array containing a string encoded as hamming code.
	 * @return Returns the string encoded as hamming code in <b>hammingMessage</b>.
	 */
	public static String extractHammingMessage(byte hammingMessage[]) {
		// bit 0 is the overall parity bit.  It stores the parity of the other 7 bits.
		// bits 1,2,4 are parity bits.  They store the parity of the data bits.
		// bits 3,5,6,7 are data bits.  They store the data.

		// we must grab bits 3,5,6,7 from each byte, put them together, then put them with the other half of the character.
		// we go through the array 2 at a time to grab bytes that make a character at the same time.

		String message = "";
		for (int i = 0; i < hammingMessage.length; i += 2) {
			byte data[] = new byte[8];
			for (int j = 0; j < 2; j++) { // once for each byte associated with each character
				byte filter = 0x10; // 0001 0000 in binary
				for (int k = 0; k < 4; k++) { // once for each data bit in each byte
					data[j * 4 + k] = (byte) ( (hammingMessage[i + j] & filter) >>> ( (k == 0)? 4 : 3 - k) );

					// shift filter according to which data bit we just read
					if (k == 0) {
						filter = (byte) (filter >>> 2);
					} else {
						filter = (byte) (filter >>> 1);
					}

					// sanity check
					if (!isSingleBit(data[j * 4 + k])) {
						System.out.printf("hammingMessage[%d]: data[%d]: %02x", i, j * 4 + k, data[j * 4 + k]);
					}
				}
			}
			byte result = 0;
			for (int j = data.length - 1; j >= 0; j--) {
				result = (byte) ( result | ( data[j] << (data.length - j - 1) ) );
			}

			// System.out.printf("0x%02x  ", result);

			message = message + (char) result;
		}

		return message;
	}

	/**
	 * Corrects any errors that exist in <b>hammingByte</b>, if possible.
	 * @param hammingByte is the byte encoded as hamming code that will be corrected if possible.
	 * @return Returns <b>hammingByte</b> with one single error corrected.  If there are no errors that exist then it returns <b>hammingByte</b>.
	 */
	private static byte correctHammingByte(byte hammingByte) {
		if(checkByteHelper(hammingByte) == 0) {
			return hammingByte;
		}
		
		byte data[] = getDataBits(hammingByte);
		byte parity[] = getParityBits(hammingByte);

		byte correctedHammingByte;

		// verify all parity bits and record all incorrect bits.
		ArrayList<Integer> incorrectParityBits = new ArrayList<Integer>();

		// 3,5,7
		if (computeParity(data[0],data[1],data[3]) != parity[1]) {
			incorrectParityBits.add(1);
		}

		// 3,6,7
		if (computeParity(data[0],data[2],data[3]) != parity[2]) {
			incorrectParityBits.add(2);
		}

		// 5,6,7
		if(computeParity(data[1],data[2],data[3]) != parity[3]) {
			incorrectParityBits.add(3);
		}

		if(incorrectParityBits.size() == 0) {
			// the incorrect bit is the overall parity bit.
			correctedHammingByte = (byte) (hammingByte ^ 0x80);
		} else {
			if(incorrectParityBits.size() == 1) {
				// only 1 of the 3 parity bits indicates an error, so that bit is in error
				int errorBit = incorrectParityBits.get(0);
				correctedHammingByte = (byte) ( hammingByte ^ ( (errorBit == 1)? 0x40 : ( (errorBit == 2)? 0x20 : 0x08) ) );
			} else {
				// 2 or more parity bits see an error so the data bit that intersects all parity bits in error is in error

				int errorBit = 0;
				for(int parityIndex: incorrectParityBits) {
					errorBit += Math.pow(2, parityIndex-1);
				}

				switch(errorBit) {
				case 3:
					correctedHammingByte = (byte) (hammingByte ^ 0x10);
					break;
				case 5:
					correctedHammingByte = (byte) (hammingByte ^ 0x04);
					break;
				case 6:
					correctedHammingByte = (byte) (hammingByte ^ 0x02);
					break;
				case 7:
					correctedHammingByte = (byte) (hammingByte ^ 0x01);
					break;
				default:
					System.out.println("Something went wrong when adding bits to find erronous bit.  Sum = " + errorBit);
					correctedHammingByte = hammingByte;
					break;
				} // end switch
			} // end else (2 or more parity bits indicates an error)
		} // end else (overall parity bit is not in error)

		return correctedHammingByte;
	}
		
	

	/**
	 * Corrects the hamming code in <b>hammingMessage</b>.  This assumes that there exists at most 1 error in each of the bytes.
	 * @param hammingMessage is the hammingCode that contains at most 1 error in each of the bytes in the array.
	 * @return returns the hammingCode from <b>hammingMessage</b> except any single errors in any of the bytes have been corrcted.  If there are no errors, then it returns <b>hammingMessage</b>.  If there are too many errors in any of the bytes for the hamming code to correct, then it returns <b>hammingMessage</b>.
	 */
	public static byte[] correctHammingCode(byte hammingMessage[]) {
		// first check if there are any errors or if they are unfixable.
		int numErrors = checkHammingCode(hammingMessage);

		byte result[];

		if(numErrors != 1) {
			// either no errors or we are unable to fix them
			result = hammingMessage;
		} else {
			// there is at most 1 error in each byte.  This is fixable.

			byte correctedHammingMessage[] = new byte[hammingMessage.length];

			for(int i = 0; i < hammingMessage.length; i++) {
				if(checkByte(hammingMessage[i]) == 0) {
					// this byte is fine
					correctedHammingMessage[i] = hammingMessage[i];
				} else {
					// this byte has an error and needs to be corrected
					correctedHammingMessage[i] = correctHammingByte(hammingMessage[i]);
				}
			}

			result = correctedHammingMessage;
		}

		return result;
	}

	/**
	 * Converts the bytes in the array <b>bytes</b> to a hex string.
	 * @param bytes is the byte array to be converted to a string.
	 * @return Returns a String containing the hex values of the bytes in the array <b>bytes</b>.
	 */
	public static String byteArrayToHexString(byte[] bytes) {
		char[] hexArray = "0123456789ABCDEF".toCharArray();
		// there are twice the number of characters in the string as there are bytes in the array because each byte is 2 characters
	    char[] hexChars = new char[bytes.length * 2]; 
		for ( int i = 0; i < bytes.length; i++ ) {
			int v = bytes[i] & 0xFF; // & 0xFF is a more efficient way of converting the result to an integer
			hexChars[i * 2] = hexArray[v >>> 4];
			hexChars[i * 2 + 1] = hexArray[v & 0x0F];
		}
		String result = new String(hexChars);
		return result;
	}

	/**
	 * Converts the hex string <b>s</b> into a corresponding array of bytes.
	 * @param s is the hex string to be converted into a byte array.
	 * @return Returns a byte array containing bytes taken from the hex values in <b>s</b>.
	 */
	public static byte[] hexStringToByteArray(String s) {
		byte[] bytes = new byte[s.length() / 2];
		for (int i = 0; i < bytes.length; i++) {
			int index = i * 2;
			// step through the string 2 characters at a time and construct use Integer.parseInt to turn the substring into an int
			int v = Integer.parseInt(s.substring(index, index + 2), 16);
			bytes[i] = (byte) v;
		}
		return bytes;
	}
	
	// public static void main(String args[]) {

	// 	// works
	// 	// byte testByteArr[] = {1, 0, 0};
	// 	// System.out.println(computeParity(testByteArr[0],testByteArr[1],testByteArr[2]));

	// 	// works
	// 	// byte testByte = (byte) 0xdf;
	// 	// System.out.printf("0x%02x", computeParity(testByte));
		
	// 	// String test = "a"; // should be 0x66 0xe9
	// 	// string test = "q"; // should be 0x8f 0xe9
	// 	// String test = "asfd"; // should be 0x66 0xe9 0x8f 0x43 0x66 0x66 0x66 0xcc

	// 	// byte testBytes[] = test.getBytes();

	// 	// System.out.println(test + " as bytes:");
	// 	// for(int i = 0; i < testBytes.length; i++) {
	// 	// 	System.out.printf("0x%02x\n", testBytes[i]);
	// 	// }

	// 	// System.out.println("\n" + test + " encoded as hamming code:");
	// 	// byte hammingCode[] = encodeHammingCode(test);
	// 	// for(int i = 0; i < hammingCode.length; i++) {
	// 	// 	System.out.printf("0x%02x\n", hammingCode[i]);
	// 	// }

	// 	// String testString = "abc"; // hamming code should be 66e9 66aa 6643
	// 	// // String testString = "123";
	// 	// byte testHammingCode[] = encodeHammingCode(testString);
	// 	// System.out.println(testString + " as hamming code: ");
	// 	// for (int i = 0; i < testHammingCode.length; i++) {
	// 	// 	System.out.printf("0x%02x  ", testHammingCode[i]);
	// 	// }
	// 	// System.out.println("\n" + testString + " in hamming code decoded:");
	// 	// String decodedHammingCode = extractHammingMessage(testHammingCode);
	// 	// System.out.println("\n" + testString + " decoded to: " + decodedHammingCode);

	//     // byte parityTest = (byte) 0xA5;
	// 	// System.out.printf("0x%02x parity: %d", parityTest, computeParity(parityTest));

	// 	// client
	// 	// String testString = "abc";
	// 	// byte testHammingBytes[] = encodeHammingCode(testString);
	// 	// System.out.println("\n" + testString + " as hamming code: ");
	// 	// for(int i = 0; i < testHammingBytes.length; i++) {
	// 	// 	System.out.printf("0x%02x\n", testHammingBytes[i]);
	// 	// }

	// 	// String transmittedTest = byteArrayToHexString(testHammingBytes);
	// 	// transmittedTest = "2" + transmittedTest;
	// 	// System.out.println("\ntestHammingBytes as a string with 2 on the front: " + transmittedTest);
	// 	// System.out.println("transmittedTest length: " + transmittedTest.length());

	// 	// // server
	// 	// String receivedTest = transmittedTest;
	// 	// receivedTest = receivedTest.substring(1);
	// 	// byte receivedHammingBytes[] = hexStringToByteArray(receivedTest);
	// 	// System.out.println("\nrecievedHammingBytes: ");
	// 	// for(int i = 0; i < receivedHammingBytes.length; i++) {
	// 	// 	System.out.printf("0x%02x\n", receivedHammingBytes[i]);
	// 	// }
		
	// 	// int errors = checkHammingCode(receivedHammingBytes);
	// 	// System.out.println("\nTotal Errors: " + errors);

		
	// }
}
