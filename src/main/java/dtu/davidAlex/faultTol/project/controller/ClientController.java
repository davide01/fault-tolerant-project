package dtu.davidAlex.faultTol.project.controller;

public interface ClientController {
	public void sendMessage();
	
	public void doErrorCheck(boolean type);
	
	public void injectError();
	
	public void setMessage(String msg);

	public void setReplaceErrors(String replace);
}
