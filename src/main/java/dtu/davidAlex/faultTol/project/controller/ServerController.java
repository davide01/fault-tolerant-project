package dtu.davidAlex.faultTol.project.controller;

public interface ServerController {
	public void setMessage(String message);
	
	public void decodeMessage();
}
